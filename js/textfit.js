jQuery.fn.textfit = function(options) {
  	// Merge passed options with defaults
  	var opts = jQuery.extend({}, jQuery.fn.textfit.defaults, options);

  	return this.each(function() {
	    // textfit!
	    var elem = $(this);
	    if (elem.height() > elem.parent().height()) {
			var newSize = parseInt(elem.css('font-size')) - opts.fontstep;
			elem.css('font-size', newSize+'px');
			elem.textfit();
		}else{
			elem.css('opacity','100');
		}
	});
};


jQuery.fn.textfit.defaults = {
  fontstep: 2,
  maxfont: 200,
  minfont: 14, 
  lineheight: 1
};