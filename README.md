# Textfit.js #

Make your text Fit where you want it!

## How it Works ##

Reduces the font size of an element until it is smaller than the height of the parent.


## How to Use ##
### Step 1 - Load the Javascript and CSS ###

Look inside the js folder to find textfit.min.js and load both of these files. Load jQuery first.
```
#!html
<script src="js/jquery.min.js"></script>
<script src="js/textfit.min.js"></script>
```
Look inside the css folder to find textfit.css and load it.
```
#!html
<link href="css/textfit.css" rel="stylesheet" type="text/css" />	
```
### Step 2 - Call Functions ###

```
#!html

//Element class that you would like to resize.
$('.textfit').textfit(options);
```
### Step 3 - Enjoy! ###

## Default Options ##
```
#!html
var options = {
    fontstep: 2, //font will decrease by 2 until it fits.
    minfont: 14, //font will not get smaller than this.
    lineheight: 1 
};
$('.textfit').textfit(options);
```